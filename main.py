import time

file_list = ['a.txt', 'b.txt']


def number_extractor_gen(lines: str):
    '''
    Генератор, который возвращает нам элементы из массива строк в типе int (можно было не этот цикл засунуть и в
    основной, просто так красивее)
    :param lines: массив строк с числами
    :return: массив чисел
    '''
    strings = lines.split('\n')
    n = int(strings[0])
    for i in range(1, n + 1):
        yield int(strings[i])


def main(file_name):
    '''
    Обрабатываем файл
    :param file_name: имя файла
    :return: строка ответ
    '''
    # читаем сразу весь файл в оперативку - так быстрее чем, читать по строкам
    with open(file_name) as f:
        # инициализируем генератор
        numbers = number_extractor_gen(f.read())
    # счетчик цикла
    iterator = 1
    # массивы с суммами и длинами подпоследовательностей, в котороых номер элемента соотвествует остатку от деления на 43
    sums = []
    lens = []
    # макисмальная сумма и минимальная длина
    max_sum = 0
    min_len = 0
    # текущая вычисляемая сумма
    current_sum = 0

    # инициализируем массивы, число 100000000001 - максимально возможная сумма + 1
    for i in range(0,  43):
        sums.append(100000000001)
        lens.append(0)

    # цикл итерирует по всем числам из файла
    for number in numbers:
        current_sum += number
        mod = current_sum % 43

        if mod == 0:
            max_sum = current_sum
            min_len = iterator
        else:
            diff_sum = current_sum - sums[mod]
            diff_len = iterator - lens[mod]

            if diff_sum > max_sum:
                max_sum = diff_sum
                min_len = diff_len
            if diff_sum == max_sum:
                min_len = diff_len if diff_len < min_len else min_len
            if current_sum < sums[mod]:
                sums[mod] = current_sum
                lens[mod] = iterator
            iterator += 1

    return f'файл: {file_name}, Сумма: {max_sum}, длинна: {min_len}'


if __name__ == '__main__':
    tic = time.perf_counter()
    for file in file_list:
        print(main(file))
    toc = time.perf_counter()
    print(f'Время выполнения проги: {toc - tic} сек')
